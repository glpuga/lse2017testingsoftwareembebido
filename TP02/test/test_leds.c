#include "unity.h"
#include "leds.h"
#include <string.h>

void setUp(void) {
}

void tearDown(void) {
}

void test_LedsOffAfterCreate(void) 
{
   uint16_t ledsVirtuales = 0xFFFF;

   Leds_Create(&ledsVirtuales);

   TEST_ASSERT_EQUAL_HEX16(0x0000, ledsVirtuales);
}

void test_TurnOnLedOne(void)
{
   uint16_t ledsVirtuales;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0x0001);

   TEST_ASSERT_EQUAL_HEX16(0x0001, ledsVirtuales);
}

void test_TurnOnLedTwo(void)
{
   uint16_t ledsVirtuales;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0x0001);

   Leds_TurnOn(0x0010);

   TEST_ASSERT_EQUAL_HEX16(0x0011, ledsVirtuales);
}

void test_TurnOffLedOne(void)
{
   uint16_t ledsVirtuales;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0x0001);

   Leds_TurnOff(0x0001);

   TEST_ASSERT_EQUAL_HEX16(0x0000, ledsVirtuales);
}

void test_TurnOnLedMany(void)
{
   uint16_t ledsVirtuales;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0x0101);

   TEST_ASSERT_EQUAL_HEX16(0x0101, ledsVirtuales);
}

void test_TurnOffLedMany(void)
{
   uint16_t ledsVirtuales;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0x0111);

   Leds_TurnOff(0x0101);

   TEST_ASSERT_EQUAL_HEX16(0x0010, ledsVirtuales);
}

void test_TurnOnLedAll(void)
{
   uint16_t ledsVirtuales;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0xffff);

   TEST_ASSERT_EQUAL_HEX16(0xffff, ledsVirtuales);
}

void test_TurnOffLedAll(void)
{
   uint16_t ledsVirtuales;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0xffff);

   Leds_TurnOff(0xffff);

   TEST_ASSERT_EQUAL_HEX16(0x0000, ledsVirtuales);
}

void test_GetLedsStatus(void)
{
   uint16_t ledsVirtuales;
   int statusOne;
   int statusTwo;

   Leds_Create(&ledsVirtuales);

   Leds_TurnOn(0x0001);

   Leds_GetStatus(0x0001, &statusOne);

   Leds_TurnOff(0x0001);

   Leds_GetStatus(0x0001, &statusTwo);

   TEST_ASSERT_EQUAL_HEX16(0x0001, statusOne);
   TEST_ASSERT_EQUAL_HEX16(0x0000, statusTwo);
}

void test_CheckParametersCreateOk(void)
{
   uint16_t ledsVirtuales;
   int returnValue;

   returnValue = Leds_Create(&ledsVirtuales);

   TEST_ASSERT_EQUAL_INT(0, returnValue);
}

void test_CheckParametersCreateFail(void)
{
   int returnValue;

   returnValue = Leds_Create(NULL);

   TEST_ASSERT_EQUAL_INT(-1, returnValue);
}

void test_CheckParametersTurnOn(void)
{
   uint16_t ledsVirtuales;
   int returnValue;

   returnValue = Leds_Create(&ledsVirtuales);

   returnValue = Leds_TurnOn(0x0001);
   TEST_ASSERT_EQUAL_INT(0, returnValue);

   returnValue = Leds_TurnOn(0x8000);
   TEST_ASSERT_EQUAL_INT(0, returnValue);

   returnValue = Leds_TurnOn(0x10000);
   TEST_ASSERT_EQUAL_INT(-1, returnValue);

   returnValue = Leds_TurnOn(-1);
   TEST_ASSERT_EQUAL_INT(-1, returnValue);
}

void test_CheckParametersTurnOff(void)
{
   uint16_t ledsVirtuales;
   int returnValue;

   returnValue = Leds_Create(&ledsVirtuales);

   returnValue = Leds_TurnOff(0x0001);
   TEST_ASSERT_EQUAL_INT(0, returnValue);

   returnValue = Leds_TurnOff(0x8000);
   TEST_ASSERT_EQUAL_INT(0, returnValue);

   returnValue = Leds_TurnOff(0x10000);
   TEST_ASSERT_EQUAL_INT(-1, returnValue);

   returnValue = Leds_TurnOff(-1);
   TEST_ASSERT_EQUAL_INT(-1, returnValue);
}

void test_CheckParametersGetValue(void)
{
   uint16_t ledsVirtuales;
   int ledsStatus;
   int returnValue;

   returnValue = Leds_Create(&ledsVirtuales);

   returnValue = Leds_GetStatus(0x0001, &ledsStatus);
   TEST_ASSERT_EQUAL_INT(0, returnValue);

   returnValue = Leds_GetStatus(0x8000, &ledsStatus);
   TEST_ASSERT_EQUAL_INT(0, returnValue);

   returnValue = Leds_GetStatus(0x10000, &ledsStatus);
   TEST_ASSERT_EQUAL_INT(-1, returnValue);

   returnValue = Leds_GetStatus(-1, &ledsStatus);
   TEST_ASSERT_EQUAL_INT(-1, returnValue);
}


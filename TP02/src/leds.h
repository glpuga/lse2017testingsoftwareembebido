#include <stdint.h>

int Leds_Create(uint16_t * direccion);

int Leds_TurnOn(int led);

int Leds_TurnOff(int led);

int Leds_GetStatus(int led, int *status);
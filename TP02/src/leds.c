#include "leds.h"
#include <string.h>

static uint16_t * leds;

int Leds_Create(uint16_t * direccion) 
{
   int returnValue;

   returnValue = 0;

   if (direccion != NULL)
   {
      leds = direccion;
      *leds = 0;

   } else {

      returnValue = -1;
   }

   return returnValue;
}

int Leds_TurnOn(int led) 
{
   int returnValue;

   returnValue = 0;

   if ((led > 0xffff) || (led < 0))
   {
      returnValue = -1;

   } else {

      *leds = *leds | led;
   }

   return returnValue;
}

int Leds_TurnOff(int led) 
{
   int returnValue;

   returnValue = 0;

   if ((led > 0xffff) || (led < 0))
   {
      returnValue = -1;

   } else {

      *leds = *leds & (~led);
   }

   return returnValue;
}

int Leds_GetStatus(int led, int *status) 
{
   int returnValue;

   returnValue = 0;

   if ((led > 0xffff) || (led < 0))
   {
      returnValue = -1;

   } else {

      *status = *leds & led;
   }

   return returnValue;
}

/**
 * @file bateria.c
 * @date 28/06/2017
 * @author Ing. Alejandro Celery
 * @brief Implementacion de las funciones de la Bateria.
 */

#include "bateria.h"
#include "analog.h"
#include "Framework.h"

/**
 * Inicializa el modulo y lo programa para verificar la condicion de arranque en periodoMedicionBateriaInicial ms.
 * @see verificaCondicionDeArranque
 */

void Bateria_Init							( Bateria * bateria )
{
   bateria->estado = sBATERIA_LLENA;
}

/**
 * Senial | Valor | Evento
 * -------|-------|-------
 * SIG_TIMEOUT (primera ocurrencia) | x | Una vez por segundo. Debe medir la tension y tomar las acciones correspondientes.
 * SIG_ARRANQUE | x | Cuando se esta produciendo un arranque del motor. Debe suspender las mediciones de bateria durante 60 segundos.
 */
void Bateria_manejadorEventos 				( Bateria * bateria, Evento * evento )
{
   // Evento evn;
   Evento evn;

   int tensionMedida;

   switch( evento->senial )
   {
      case SIG_TIMEOUT:
         tensionMedida = Bateria_tomarUnaMedicion( bateria );

         if(tensionMedida >= uBATERIA_CARGANDO)
         {
            bateria->estado = sBATERIA_CARGANDO;

         } else {

            if ((tensionMedida >= uBATERIA_LLENA) && (bateria->estado <= sBATERIA_LLENA))
            {
               bateria->estado = sBATERIA_LLENA;

            }  else {

               if ((tensionMedida >= uBATERIA_MEDIA) && (bateria->estado <= sBATERIA_MEDIA))
               {
                  bateria->estado = sBATERIA_MEDIA;

               } else {

                  if ((tensionMedida >= uBATERIA_BAJA) && (bateria->estado <= sBATERIA_BAJA))
                  {

                     /*
                      * Al entrar por primera vez inicializo el
                      * contador de segundos que uso para mantener
                      * el led alternando.
                      */
                     if (bateria->estado < sBATERIA_BAJA)
                     {
                        bateria->tiempoCritico = 0;
                     } else {
                        bateria->tiempoCritico++;
                     }

                     bateria->estado = sBATERIA_BAJA;

                  } else {

                     if ((tensionMedida >= uBATERIA_VACIA) && (bateria->estado <= sBATERIA_VACIA))
                     {
                        bateria->estado = sBATERIA_VACIA;

                     } else {


                        if (bateria->estado < sBATERIA_CRITICA)
                        {
                           /*
                            * Primera vez que entra en BATERIA_CRITICA, inicio el contador.
                            *
                            * */
                           bateria->tiempoCritico = 60;

                        } else {

                           /*
                            * Entrada repetida en BATERIA CRITICA, así que actualizo
                            * el contador en función de su valor actual.
                            * */

                           bateria->tiempoCritico--;
                        }

                        bateria->estado = sBATERIA_CRITICA;

                        if ((bateria->tiempoCritico % 10) == 0)
                        {
                           evn.senial = SIG_BATERIA_CRITICA;
                           evn.valor = bateria->tiempoCritico;

                           Framework_publicarEvento(&evn);
                        }

                        if (bateria->tiempoCritico == 0)
                        {
                           evn.senial = SIG_APAGAR_ACCESORIOS;
                           evn.valor = 0;

                           Framework_publicarEvento(&evn);
                        }
                     }
                  }
               }
            }
         }

         switch (bateria->estado) {
            case sBATERIA_CARGANDO  :
            case sBATERIA_LLENA :
            case sBATERIA_MEDIA :
               setEstadoLEDPanelFrontal(0);
               break;

            case sBATERIA_BAJA :
               setEstadoLEDPanelFrontal((bateria->tiempoCritico + 1) % 2);
               break;

            case sBATERIA_VACIA :
            case sBATERIA_CRITICA :
               setEstadoLEDPanelFrontal(1);
               break;
         }
         break;

            case SIG_ARRANQUE:
            default:
               break;
   }
}

int  Bateria_tomarUnaMedicion(Bateria * bateria)
{
   int medidasValidas;
   int medidasAcumuladas;
   int medidaPuntual;

   medidasValidas = 0;
   medidasAcumuladas = 0;

   while(medidasValidas < 2)
   {
      medidaPuntual = getValorInstantaneoPinBateria( ) * ESCALA_CUENTAS_MV;

      if ((medidaPuntual >= 6000) && (medidaPuntual <= 18000))
      {
         medidasValidas++;
         medidasAcumuladas += medidaPuntual;
      }
   }

   return medidasAcumuladas / medidasValidas;
}
